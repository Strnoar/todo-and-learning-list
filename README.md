# 💡 Todo and Learning List

This project helps me to manage my learning list


## Backend
*  Java/Kotlin app
*  Deno/Typescript app

## Mobile
*  Flutter app
*  Android native app

## Frontend
*  React.js
*  Typescript

## Architecture
*  Event-sourcing
*  DDD